﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CardioDiagnostics.Models;
using Highsoft.Web.Mvc.Stocks;

namespace CardioDiagnostics.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
    
        public ActionResult Index()
        {
            if (!Request.IsAuthenticated)
            {
                return RedirectToRoute(new
                {
                    controller = "Account",
                    action = "Login",

                });
            }
            IEnumerable<Device> devices = db.Devices.ToList();
            List <List<LineSeriesData>> listOfCharts = new List<List<LineSeriesData>>();
            List<Device> displayedDevices = new List <Device> ();
            foreach (Device device in devices)
            {
                List<LineSeriesData> chartData = new List<LineSeriesData>();
                IEnumerable<Data> deviceData = db.Data.SqlQuery("SELECT * FROM Data WHERE Device_Id = " + device.Id);
                foreach(Data data in deviceData)
                {
                    DateTime dt = Convert.ToDateTime(data.Date_Recorded);
                    long timestamp = (long)(dt - new DateTime(1970, 1, 1)).TotalMilliseconds;
                    chartData.Add(new LineSeriesData { X = timestamp, Y = data.Value });
                }
                if (chartData.Count() > 0)
                {
                    listOfCharts.Add(chartData);
                    displayedDevices.Add(device);
                }
            }
            ViewData["listOfCharts"] = listOfCharts;
            ViewData["displayedDevices"] = displayedDevices;

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Login()
        {
            ViewBag.Message = "Your application Login page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}