﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CardioDiagnostics.Models;
using System.DirectoryServices;
using System.IO;
using System.Data.Entity;


namespace CardioDiagnostics.Controllers
{
    public class DataController : Controller
    {
        public ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            return View(new List<Data>());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Index(HttpPostedFileBase postedFile)
        {
            List<Data> Rows = new List<Data>();
            string filePath = string.Empty;
            if (postedFile != null)
            {
                string path = Server.MapPath("Uploads/");
                if (!Directory.Exists(path))
                {
                     Directory.CreateDirectory(path);
                }

                filePath = path + Path.GetFileName(postedFile.FileName);
                string extension = Path.GetExtension(postedFile.FileName);
                postedFile.SaveAs(filePath);

                //Read the contents of CSV file.
                string csvData = System.IO.File.ReadAllText(filePath);

                //Execute a loop over the rows.
                foreach (string row in csvData.Split('\n'))
                {
                    if (!string.IsNullOrEmpty(row))
                    {
                        Data dm = new Data
                        {

                            Device_Id = Convert.ToInt32(row.Split(',')[0]),
                            Value = Convert.ToInt32(row.Split(',')[1]),
                            Date_Recorded = Convert.ToString(row.Split(',')[2]),
                            Date_Added = Convert.ToString(row.Split(',')[3]),
                            
                            
                        };
                        db.Data.Add(dm);
                        db.SaveChanges();
                        
                    }
                }
            }

            return View(Rows);
        }
    }
}