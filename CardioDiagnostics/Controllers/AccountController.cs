﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CardioDiagnostics.Models;
using System.Threading.Tasks;
using System.Web.Security;

namespace CardioDiagnostics.Controllers
{
    public class AccountController : Controller
    {

        // GET: Account
        public ActionResult Index()
        {
            return View();
        }
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Login(string Email, string Password)
        {
            if (FormsAuthentication.Authenticate(Email, Password))
            {
                FormsAuthentication.SetAuthCookie(Email, false);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewData["LastLoginFailed"] = true;
                Response.Write(Email + Password);
                return View();
            }
        }
    }
}