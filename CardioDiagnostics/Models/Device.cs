namespace CardioDiagnostics.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Device
    {
        public int Id { get; set; }

        [Required]
        [StringLength(256)]
        public string ModelNumber { get; set; }

        [Required]
        [StringLength(256)]
        public string SerialNumber { get; set; }
    }
}
