﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace CardioDiagnostics.Models
{
    public class Data
    {
        public int Id { get; set; }
        public int Device_Id { get; set; }
        public int Value { get; set; }

        public String Date_Recorded { get; set; }
        public String Date_Added { get; set; }



    }
}