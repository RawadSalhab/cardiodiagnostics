﻿namespace CardioDiagnostics.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    public class ApplicationDbContext :DbContext
    {
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<CardioDiagnostics.Models.Device> Devices { get; set; }

        public System.Data.Entity.DbSet<CardioDiagnostics.Models.Patient> Patients { get; set; }

        public System.Data.Entity.DbSet<CardioDiagnostics.Models.Data> Data { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}