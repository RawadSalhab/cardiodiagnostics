namespace CardioDiagnostics.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class adddata : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Patients (Name) VALUES ('Rawad')");
            Sql("INSERT INTO Patients (Name) VALUES ('Samir')");
            Sql("INSERT INTO Patients (Name) VALUES ('Hanan')");
            Sql("INSERT INTO Patients (Name) VALUES ('Ahmad')");
            Sql("INSERT INTO Patients (Name) VALUES ('Rafi')");
        }

        public override void Down()
        {
        }
    }
}
