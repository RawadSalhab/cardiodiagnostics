namespace CardioDiagnostics.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class adddatadevices : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Devices (SerialNumber, ModelNumber) VALUES ('00001','CDiag 101')");
            Sql("INSERT INTO Devices (SerialNumber, ModelNumber) VALUES ('00002','CDiag 101')");
            Sql("INSERT INTO Devices (SerialNumber, ModelNumber) VALUES ('00003','CDiag 102')");
            Sql("INSERT INTO Devices (SerialNumber, ModelNumber) VALUES ('00004','CDiag 101')");
            Sql("INSERT INTO Devices (SerialNumber, ModelNumber) VALUES ('00005','CDiag 102')");
        }

        public override void Down()
        {
        }
    }
}
