namespace CardioDiagnostics.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class Initial : DbMigration
    {
        public override void Up()
        {

            CreateTable(
               "dbo.Patients",
               c => new
               {
                   Id = c.Int(nullable: false, identity: true),
                   Name = c.String(nullable: false, maxLength: 256),
               })
               .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Devices",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    ModelNumber = c.String(nullable: false, maxLength: 256),
                    SerialNumber = c.String(nullable: false, maxLength: 256),
                    Patient_Id = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Patients", t => t.Patient_Id, cascadeDelete: true)
                .Index(t => t.Patient_Id);

            CreateTable(
               "dbo.Datas",
               c => new
               {
                   Id = c.Int(nullable: false, identity: true),
                   Device_Id = c.Int(nullable: false),
                   Value = c.Int(nullable: false),
                   Date_Recoded = c.String(nullable: false),
                   Date_Added = c.String(nullable: false)
               })
               .PrimaryKey(t => t.Id)
               .ForeignKey("dbo.Devices", t => t.Device_Id, cascadeDelete: true)
               .Index(t => t.Device_Id);

        }

        public override void Down()
        {
            DropForeignKey("dbo.Datas", "Device_Id", "dbo.Devices");
            DropForeignKey("dbo.Devices", "Patient_Id", "dbo.Patients");
            DropIndex("dbo.Datas", new[] { "Device_Id" });
            DropIndex("dbo.Devices", new[] { "Patient_Id" });
            DropTable("dbo.Datas");
            DropTable("dbo.Devices");
            DropTable("dbo.Patients");
        }
    }
}
